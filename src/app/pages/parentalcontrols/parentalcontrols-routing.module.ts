import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentalcontrolsPage } from './parentalcontrols.page';

const routes: Routes = [
  {
    path: '',
    component: ParentalcontrolsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentalcontrolsPageRoutingModule {}
