import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentalcontrolsPageRoutingModule } from './parentalcontrols-routing.module';

import { ParentalcontrolsPage } from './parentalcontrols.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentalcontrolsPageRoutingModule
  ],
  declarations: [ParentalcontrolsPage]
})
export class ParentalcontrolsPageModule {}
