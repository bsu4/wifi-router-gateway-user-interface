import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.page.html',
  styleUrls: ['./clients.page.scss'],
})
export class ClientsPage implements OnInit {

  constructor() { }

  devices: any[] = [
    {
      icon:"logo-android",
      "name":"Finn",
      "ip":"10.0.0.23",
    },
    {
      icon:"laptop-outline",
      "name":"John",
      "ip":"10.0.0.67",
    },
    {
      icon:"logo-android",
      "name":"James",
      "ip":"10.0.0.45",
    },
  ]
  fives: any[] = [
    {
      icon:"logo-apple",
      "name":"Jonathan",
      "ip":"10.0.0.31",
    },
    {
      icon:"laptop-outline",
      "name":"Marcus",
      "ip":"10.0.0.68",
    },
    {
      icon:"logo-apple",
      "name":"James",
      "ip":"10.0.0.99",
    },
    {
      icon:"logo-apple",
      "name":"Lloyd",
      "ip":"10.0.0.102",
    },
    {
      icon:"tv-outline",
      "name":"Sony TV",
      "ip":"10.0.0.123",
    },
  ]
  wires: any[] = [
    {
      icon:"logo-windows",
      "name":"Eula",
      "ip":"10.0.0.144",
    },
    {
      icon:"logo-windows",
      "name":"Amber",
      "ip":"10.0.0.112",
    },
    {
      icon:"logo-playstation",
      "name":"Jawo",
      "ip":"10.0.0.115",
    },
  ]

  ngOnInit() {
  }

}
