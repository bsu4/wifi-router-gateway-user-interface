import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QuicksetupPageRoutingModule } from './quicksetup-routing.module';

import { QuicksetupPage } from './quicksetup.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QuicksetupPageRoutingModule
  ],
  declarations: [QuicksetupPage]
})
export class QuicksetupPageModule {}
