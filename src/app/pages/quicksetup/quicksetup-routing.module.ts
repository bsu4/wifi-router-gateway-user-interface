import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuicksetupPage } from './quicksetup.page';

const routes: Routes = [
  {
    path: '',
    component: QuicksetupPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuicksetupPageRoutingModule {}
