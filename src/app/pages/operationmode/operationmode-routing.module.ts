import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OperationmodePage } from './operationmode.page';

const routes: Routes = [
  {
    path: '',
    component: OperationmodePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OperationmodePageRoutingModule {}
