import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OperationmodePageRoutingModule } from './operationmode-routing.module';

import { OperationmodePage } from './operationmode.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OperationmodePageRoutingModule
  ],
  declarations: [OperationmodePage]
})
export class OperationmodePageModule {}
