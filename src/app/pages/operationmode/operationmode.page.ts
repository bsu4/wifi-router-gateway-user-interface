import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-operationmode',
  templateUrl: './operationmode.page.html',
  styleUrls: ['./operationmode.page.scss'],
})
export class OperationmodePage implements OnInit {

  constructor() { }

  modes: any[] =[
    {
      "name":"Wireless Router",
      "des":"Enables multiple users to share an Ethernet WAN (EWAN) internet connection.",
      "pic":"/assets/pics/wirelessrouter.png",
    },
    {
      "name":"Access Point",
      "des":"Transform a wired network to a wireless one.",
      "pic":"/assets/pics/accesspoint.png",
    },
    {
      "name":"Range Extender",
      "des":"Expands the wireless coverage of the existing network.",
      "pic":"/assets/pics/rangeextender.png",
    },
  ]
  ngOnInit() {
  }

}
