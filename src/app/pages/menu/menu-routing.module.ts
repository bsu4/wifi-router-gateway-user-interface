import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children:[
      {
        path:'status',
        children:[
          {
            path:'',
            loadChildren: ()=> import('../status/status.module').then( m =>m.StatusPageModule)
          }
        ]
      },
      {
        path:'clients',
        children:[
          {
            path:'',
            loadChildren: ()=> import('../clients/clients.module').then(m => m.ClientsPageModule)
          }
        ]
      },
      {
        path:'tools',
        children:[
          {
            path:'',
            loadChildren: ()=> import('../tools/tools.module').then(m => m.ToolsPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo:'pages/tabs/menu',
        pathMatch:'full'
      }
    ]
  },
  {
    path: '',
    redirectTo:'/tabs/menu',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
