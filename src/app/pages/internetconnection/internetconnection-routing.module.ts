import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InternetconnectionPage } from './internetconnection.page';

const routes: Routes = [
  {
    path: '',
    component: InternetconnectionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InternetconnectionPageRoutingModule {}
