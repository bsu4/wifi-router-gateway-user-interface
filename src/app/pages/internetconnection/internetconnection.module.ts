import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InternetconnectionPageRoutingModule } from './internetconnection-routing.module';

import { InternetconnectionPage } from './internetconnection.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InternetconnectionPageRoutingModule
  ],
  declarations: [InternetconnectionPage]
})
export class InternetconnectionPageModule {}
