import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-allowedwebsite',
  templateUrl: './allowedwebsite.page.html',
  styleUrls: ['./allowedwebsite.page.scss'],
})
export class AllowedwebsitePage implements OnInit {

  constructor() { }

  websites: any[] =[
    {
      "url":"www.facebook.com",
      icon:"logo-facebook",
    },
    {
      "url":"www.youtube.com",
      icon:"logo-youtube",
    },
    {
      "url":"www.messenger.com",
      icon:"browsers",
    },
    {
      "url":"www.tiktok.com",
      icon:"logo-tiktok",
    },
    {
      "url":"www.twitter.com",
      icon:"logo-twitter",
    },
    {
      "url":"www.instagram.com",
      icon:"logo-instagram",
    },
    {
      "url":"www.lazada.com.ph",
      icon:"browsers",
    },
    {
      "url":"www.amazon.com",
      icon:"logo-amazon",
    },
    {
      "url":"www.reddit.com",
      icon:"logo-reddit",
    },
  ]

  ngOnInit() {
  }

}
