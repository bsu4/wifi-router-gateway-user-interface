import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AllowedwebsitePageRoutingModule } from './allowedwebsite-routing.module';

import { AllowedwebsitePage } from './allowedwebsite.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AllowedwebsitePageRoutingModule
  ],
  declarations: [AllowedwebsitePage]
})
export class AllowedwebsitePageModule {}
