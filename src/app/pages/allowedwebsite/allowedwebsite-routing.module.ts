import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllowedwebsitePage } from './allowedwebsite.page';

const routes: Routes = [
  {
    path: '',
    component: AllowedwebsitePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AllowedwebsitePageRoutingModule {}
