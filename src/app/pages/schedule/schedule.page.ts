import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.page.html',
  styleUrls: ['./schedule.page.scss'],
})
export class SchedulePage implements OnInit {

  constructor() { }
  days: any[] =[
    {
      "name":"Monday",
    },
    {
      "name":"Tuesday",
    },
    {
      "name":"Wednesday",
    },
    {
      "name":"Thursday",
    },
    {
      "name":"Friday",
    },
    {
      "name":"Saturday",
    },
    {
      "name":"Sunday",
    },
  ]

  ngOnInit() {
  }

}
