import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WirelessPageRoutingModule } from './wireless-routing.module';

import { WirelessPage } from './wireless.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WirelessPageRoutingModule
  ],
  declarations: [WirelessPage]
})
export class WirelessPageModule {}
