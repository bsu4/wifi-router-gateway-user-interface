import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WirelessPage } from './wireless.page';

const routes: Routes = [
  {
    path: '',
    component: WirelessPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WirelessPageRoutingModule {}
