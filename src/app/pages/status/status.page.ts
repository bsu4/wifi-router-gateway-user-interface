import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-status',
  templateUrl: './status.page.html',
  styleUrls: ['./status.page.scss'],
})
export class StatusPage implements OnInit {

  constructor() { }

  abouts: any[] = [
    {
      "class":"Name",
      "spec":"InfoTech Bytes",
    },
    {
      "class":"Model",
      "spec":"IB840 Dual Band Wi-Fi",
    },
    {
      "class":"Firmware Version",
      "spec":"2.1.4 Build 206793",
    },
    {
      "class":"Hardware Version",
      "spec":"InfoTech Bytes v2.0",
    },
    {
      "class":"IP Address",
      "spec":"10.0.0.56",
    },
    {
      "class":"Status",
      "spec":"Connected",
    },
  ]
  ngOnInit() {
  }

}
