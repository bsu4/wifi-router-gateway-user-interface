import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SharewifiPageRoutingModule } from './sharewifi-routing.module';

import { SharewifiPage } from './sharewifi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharewifiPageRoutingModule
  ],
  declarations: [SharewifiPage]
})
export class SharewifiPageModule {}
