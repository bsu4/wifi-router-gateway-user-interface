import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SharewifiPage } from './sharewifi.page';

const routes: Routes = [
  {
    path: '',
    component: SharewifiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SharewifiPageRoutingModule {}
