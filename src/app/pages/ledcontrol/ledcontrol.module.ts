import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LedcontrolPageRoutingModule } from './ledcontrol-routing.module';

import { LedcontrolPage } from './ledcontrol.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LedcontrolPageRoutingModule
  ],
  declarations: [LedcontrolPage]
})
export class LedcontrolPageModule {}
