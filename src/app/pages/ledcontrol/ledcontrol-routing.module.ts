import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LedcontrolPage } from './ledcontrol.page';

const routes: Routes = [
  {
    path: '',
    component: LedcontrolPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LedcontrolPageRoutingModule {}
