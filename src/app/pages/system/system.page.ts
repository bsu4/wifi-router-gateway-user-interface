import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-system',
  templateUrl: './system.page.html',
  styleUrls: ['./system.page.scss'],
})
export class SystemPage implements OnInit {

  constructor(public actionSheetCtrl: ActionSheetController) { }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'This device will restart and you will lose internet connection temporally. Reboot now?',
      buttons:[
        {
          text: 'Reboot',
          role: 'destructive',
          icon: 'sync-circle',
          handler:() => {
            console.log('reboot has been clicked');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: 'close',
          handler:() => {
            console.log('reboot cancelled');
          }
        }
      ],
      cssClass: 'custom-css',
      animated: true,
      backdropDismiss: true,
      keyboardClose: false,
      mode: 'ios',
    });

    actionSheet.present();

    actionSheet.onWillDismiss().then(() => {
      console.log('action sheet is about to close');
    });
    actionSheet.onDidDismiss().then(() => {
      console.log('action sheet is about to close');
    });
  }
  async logOut() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Are you sure you want to log out?',
      buttons:[
        {
          text: 'Logout',
          role: 'destructive',
          icon: 'arrow-redo',
          handler:() => {
            console.log('You are logged out!');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: 'close',
          handler:() => {
            console.log('Logout cancel');
          }
        }
      ],
      cssClass: 'custom-css',
      animated: true,
      backdropDismiss: true,
      keyboardClose: false,
      mode: 'ios',
    });

    actionSheet.present();

    actionSheet.onWillDismiss().then(() => {
      console.log('action sheet is about to close');
    });
    actionSheet.onDidDismiss().then(() => {
      console.log('action sheet is about to close');
    });
  }

  ngOnInit() {
  }

}
