import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'menu',
    loadChildren: () => import('./pages/menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'clients',
    loadChildren: () => import('./pages/clients/clients.module').then( m => m.ClientsPageModule)
  },
  {
    path: 'tools',
    loadChildren: () => import('./pages/tools/tools.module').then( m => m.ToolsPageModule)
  },
  {
    path: 'status',
    loadChildren: () => import('./pages/status/status.module').then( m => m.StatusPageModule)
  },
  {
    path: 'quicksetup',
    loadChildren: () => import('./pages/quicksetup/quicksetup.module').then( m => m.QuicksetupPageModule)
  },
  {
    path: 'wireless',
    loadChildren: () => import('./pages/wireless/wireless.module').then( m => m.WirelessPageModule)
  },
  {
    path: 'internetconnection',
    loadChildren: () => import('./pages/internetconnection/internetconnection.module').then( m => m.InternetconnectionPageModule)
  },
  {
    path: 'parentalcontrols',
    loadChildren: () => import('./pages/parentalcontrols/parentalcontrols.module').then( m => m.ParentalcontrolsPageModule)
  },
  {
    path: 'schedule',
    loadChildren: () => import('./pages/schedule/schedule.module').then( m => m.SchedulePageModule)
  },
  {
    path: 'allowedwebsite',
    loadChildren: () => import('./pages/allowedwebsite/allowedwebsite.module').then( m => m.AllowedwebsitePageModule)
  },
  {
    path: 'qos',
    loadChildren: () => import('./pages/qos/qos.module').then( m => m.QosPageModule)
  },
  {
    path: 'guest',
    loadChildren: () => import('./pages/guest/guest.module').then( m => m.GuestPageModule)
  },
  {
    path: 'sharewifi',
    loadChildren: () => import('./pages/sharewifi/sharewifi.module').then( m => m.SharewifiPageModule)
  },
  {
    path: 'ledcontrol',
    loadChildren: () => import('./pages/ledcontrol/ledcontrol.module').then( m => m.LedcontrolPageModule)
  },
  {
    path: 'operationmode',
    loadChildren: () => import('./pages/operationmode/operationmode.module').then( m => m.OperationmodePageModule)
  },
  {
    path: 'system',
    loadChildren: () => import('./pages/system/system.module').then( m => m.SystemPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
